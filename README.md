This project is used to host a demo educational app 'My Contacts'.

It will be used to lead the students, working ideally in groups of two with a laptop per pair, to develop a simple web application using Java, HTML and a database. The application will allow them to store the contact details of their friends and family.

To do this, we will need to code the following components in the session: -

1.	Two simple HTML web pages – one to enter details and the other to display all details. (Presentation)
2.	Some simple business Java classes to code the flow through the application. (Business)
3.	A simple database with 1 table to hold the data. (Storage)

This is web development using a classic three-tier architecture (Presentation-Business-Storage), which is a standard industry pattern and for which there is a huge demand, so I thought that this could be a good use of our time together.

The main web page will show their contacts in a table with an 'Add Contact' button which can be used to add more contacts.

When the user click “Add Contact”, there will be a simple modal pop up box in which they can add the deatils of a new entry.

The application is initially quite basic, but is something that is realistic to be achieved in a single session of 2 or so hours, whioch is ideal for a school workshop.

Extra time can be used to make it a bit more striking with a few images and extra features. But the key thing is that the principles taught in this demo can be used to build much more complex, feature rich applications by following the same patterns.